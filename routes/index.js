var express = require('express');
var router = express.Router();

var Users = require('../controllers/userController')
var Friends = require('../controllers/friendsController')
var MyCalendar = require("../controllers/myCalendarController")
var Celebrities = require("../controllers/celebritiesController")

router.get('/users', Users.getAll)
router.get('/users/:id', Users.getOne)
router.post('/userwithemailandpassword', Users.getByEmailAndPassword)
router.post('/users/login', Users.login)
router.post('/users', Users.create)
router.post('/updatepassword/:id', Users.update)
router.post('/resetpassword', Users.resetpassword)

router.post('/friends', Friends.create)
router.post('/friends/:id', Friends.update)
router.get('/friends', Friends.getAll)

router.post('/myCalendar', MyCalendar.create)
router.post('/myCalendar/:id', MyCalendar.update)
router.get('/myCalendar', MyCalendar.getAll)

router.post('/celebrities', Celebrities.create)
router.post('/celebrities/:id', Celebrities.update)
router.get('/celebrities', Celebrities.getAll)

module.exports = router;