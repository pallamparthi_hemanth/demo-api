const query = require('../database/db-connection');
const {multipleColumnSet,multipleColumnUpdate,} = require('../utils/common.utils');
//const Role = require('../utils/userRoles.utils');
class UserModel {
  tableName = 'users';

  find = async (params = {}) => {
    //  console.log(params)
    let sql = `SELECT * FROM ${this.tableName}`;

    if (!Object.keys(params).length) {
      return await query(sql);
    }

    const { columnSet, values } = multipleColumnSet(params)
    sql += ` WHERE ${columnSet}`;
    // console.log(sql)
    //console.log(values)
    return await query(sql, [...values]);
  }

  findOne = async (params) => {
    const { columnSet, values } = multipleColumnSet(params)

    const sql = `SELECT * FROM ${this.tableName}
        WHERE ${columnSet}`;

    const result = await query(sql, [...values]);

    // return back the first row (user)
    return result[0];
  }

  create = async ($data) => {
    const sql = "INSERT INTO users  SET ? "

    const result = await query(sql,$data);
    const affectedRows = result ? result.affectedRows : 0;

    return affectedRows;
  }

  update = async (params, id) => {
    const { columnSet, values } = multipleColumnUpdate(params)

    const sql = `UPDATE users SET ${columnSet} WHERE id = ?`;
    const result = await query(sql, [...values, id]);

    return result;
  }
  resetpassword = async (params,email) => {
    const { columnSet, values } = multipleColumnSet(params)

    const sql = `UPDATE users SET ${columnSet}  WHERE email= ?`;

    const result = await query(sql,[...values, email]);

    return result;
  }

  // delete = async (id) => {
  //     const sql = `DELETE FROM ${this.tableName}
  //     WHERE id = ?`;
  //     const result = await query(sql, [id]);
  //     const affectedRows = result ? result.affectedRows : 0;

  //     return affectedRows;
  // }
}

module.exports = new UserModel;
