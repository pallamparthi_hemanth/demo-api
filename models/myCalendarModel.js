const query = require('../database/db-connection');
const { multipleColumnSet,multipleColumnUpdate } = require('../utils/common.utils');
//const Role = require('../utils/userRoles.utils');
class MyCalendarModel {
    tableName = 'my_calendar';

    find = async (params = {}) => {
      //  console.log(params)
        let sql = `SELECT * FROM ${this.tableName}`;

        if (!Object.keys(params).length) {
            return await query(sql);
       }

        const { columnSet, values } = multipleColumnSet(params)
        sql += ` WHERE ${columnSet}`;
         // console.log(sql)
          //console.log(values)
        return await query(sql, [...values]);
    }

    findOne = async (params) => {
        const { columnSet, values } = multipleColumnSet(params)

        const sql = `SELECT * FROM ${this.tableName}
        WHERE ${columnSet}`;

        const result = await query(sql, [...values]);

        // return back the first row (user)
        return result[0];
    }

    create = async ($data) => {
        const sql = "INSERT INTO my_calendar  SET ? "
        
        const result = await query(sql,$data);
        const affectedRows = result ? result.affectedRows : 0;

        return affectedRows;
    }

    update = async (params, id) => {
        const { columnSet, values } = multipleColumnUpdate(params)
        console.log(params)
        const sql = `UPDATE my_calendar SET ${columnSet} WHERE id = ?`;
console.log(sql,values,id)
        const result = await query(sql, [...values, id]);

        return result;
    }
    
    // delete = async (id) => {
    //     const sql = `DELETE FROM ${this.tableName}
    //     WHERE id = ?`;
    //     const result = await query(sql, [id]);
    //     const affectedRows = result ? result.affectedRows : 0;

    //     return affectedRows;
    // }
}

module.exports = new MyCalendarModel;
