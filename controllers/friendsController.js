const FriendsModel = require('../models/friendsModel');

var friendsController ={
    getAll: async function (req, res) {
       // console.log("getAll")
        let friendsList = await FriendsModel.find();
        res.status(200).send(friendsList)
    },
    
    // getOne: async function (req, res) {
    //     let userList = await UserModel.findOne(req.params);
    //     res.status(200).send(userList)
    // },
   
    create: async function (req, res) {
       

          if(req.body.image_url){
            console.log(req.body.image_url)
        }
         res.json({"created":true})

        const result = await FriendsModel.create([req.body]);

       
      
        if (!result) {
            res.status(500).send('SomeThing Went Wrong');
      
        }else{
           
            res.json({"created":true})
        }
        
       
    },
 
    update: async function (req, res) {
       console.log(req.body)
       console.log(req.params)
       let $id=Number(req.params.id)
       console.log($id)
       if(req.body.id)
       delete req.body.id;
       const result = await FriendsModel.update(req.body,$id);
       console.log(result)
      res.json({"ok":true})
    }
};
module.exports = friendsController