const UserModel = require('../models/userModel');
var PasswordGenerator = require('../services/passwordGenerator')
const emailService = require('../services/emailservice')
var userController ={
    getAll: async function (req, res) {
       // console.log("getAll")
        let userList = await UserModel.find();
        res.status(200).send(userList)
    },
    
    getOne: async function (req, res) {
        let userList = await UserModel.findOne(req.params);
        res.status(200).send(userList)
    },
    getByEmailAndPassword: async function (req, res) {
      //  console.log("for Login",req.body)
        let result = await UserModel.find(req.body);
        console.log(result)
        if (!result) {
            res.status(500).send('SomeThing Went Wrong');
      
        }
        
        res.status(200).send(result)
    },
    login: async function (req, res) {
         console.log("for Login")
        let result = await UserModel.find(req.body);
        console.log(req.body)
        if (!result) {
            res.status(500).send('SomeThing Went Wrong');
      
        }
        
        res.status(200).send(result)
    },
    create: async function (req, res) {
        let password = PasswordGenerator.generatePassword()
        req.body.password=password;
        const result = await UserModel.create([req.body]);
       // console.log(req.body)
        if (!result) {
            res.status(500).send('SomeThing Went Wrong');
      
        }else{
            emailService.sendPassword(req.body)
            res.json({"ok":true})
        }
        
       
    },
    resetpassword: async function (req, res) {
        let password = PasswordGenerator.generatePassword()
        req.body.password=password;
        const result = await UserModel.resetpassword({"password":password},req.body.email);
       // console.log(req.body)
        if (!result) {
            res.status(500).send('SomeThing Went Wrong');
      
        }else{
            emailService.sendPassword(req.body)
            res.json({"ok":true})
        }
        
       
    },
    update: async function (req, res) {
       console.log(req.body)
       console.log(req.params)
       let $id=Number(req.params.id)
       console.log($id)
       const result = await UserModel.update(req.body,$id);
       console.log(result)
      res.json({"ok":true})
    }
};
module.exports = userController